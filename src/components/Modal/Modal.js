import React from "react";

import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

import {connect} from "react-redux";
import {setHidden, setMessageText} from "../../store/actions/editModal";


function EditModal(props){

	function onInput(e){
		props.setText(e.target.value);
	}

	function handleChange(){
		props.editMessage(props.currMessageId, props.messageText);
		props.setHidden();
	}

	function exitEditMode(){
		props.setText();
		props.setHidden();
	}

	return (

		<Modal
			show={props.show }
			onHide={exitEditMode}
			backdrop="static"
			keyboard={false}
		>
			<Modal.Header closeButton>
				<Modal.Title>Modal title</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<input type="text" onChange={onInput} value={props.messageText || ""}/>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={exitEditMode}>
					Close
				</Button>
				<Button variant="primary" onClick={handleChange}>Edit Message</Button>
			</Modal.Footer>
		</Modal>);
}


function mapStateToProps(state) {
	return {
		...state.editModal,
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		setHidden: () => dispatch(setHidden()),
		setText : (t) => dispatch(setMessageText(t)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EditModal);