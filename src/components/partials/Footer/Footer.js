import React from "react";

import "./Footer.css";

export default function Footer() {
	return(<footer id={"footer"}>
		<p>Powered by binary studio tea</p>
	</footer>);
}