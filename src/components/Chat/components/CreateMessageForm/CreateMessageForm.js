import React from "react";
import "./CreateMessageForm.css";

class CreateMessageForm extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			message: props.prevText || "",
		};
	}

	handleChange = (event, name) => {
		this.setState({
			[name]: event.target.value,
		});
	};

	render() {
		return (
			// eslint-disable-next-line no-mixed-spaces-and-tabs
			<div className={"message-create-form"}>

				<textarea placeholder={"Your message..."} name={"message"} value={this.state.message}
						  onChange={(e) => this.handleChange(e, "message")}>
				</textarea>


				<button onClick={() => {
					this.props.createMessage(this.state.message);
					this.setState({message: ""});
				}}>
					Send message!
				</button>


				<button onClick={this.props.scrollChat}>
					Scroll down!
				</button>
			</div>

		);
	}
}

export default CreateMessageForm;