import React from "react";
import "./Header.css";

function Header(props) {

	const lastMessDate = new Date(props.lastMessageTime);

	return (
		<header>
			<ul>
				<li>{props.chatName || "My chat"}</li>
				<li>{props.participantsAmount || "0"} participants</li>
				<li>{props.messagesAmount || "0"} messages</li>
			</ul>

			<p>Last message at {`${lastMessDate.getHours()} : ${lastMessDate.getMinutes()}` || "00:00"}</p>
		</header>
	);


}

export default Header;