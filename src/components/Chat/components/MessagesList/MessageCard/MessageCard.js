import React from "react";

class MessageCard extends React.Component {


	shouldComponentUpdate(nextProps, nextState) {
		const {message, userId} = nextProps;
		const oldMessage = this.props.message;
		if (userId !== this.props.userId) {
			return true;
		} else if (message.text === oldMessage.text) {
			return true;
		} else if (message.likes.some(user => !oldMessage.likes.includes(user) || oldMessage.likes.some(user => !message.likes.includes(user)))) {
			// check likes
			return true;
		} else if (nextState) {
			return true;
		}
		return false;
	}

	setLike = (userId, message) => {
		if (message.likes.includes(userId)) {
			return;
		}
		message.likes.push(userId);

		this.setState({rerender: true});
	};

	deleteLike = (userId, message) => {
		message.likes = message.likes.filter(us => us !== userId);
		this.setState({rerender: true});
	};

	render() {
		const {deleteMessage, userId, message} = this.props;
		const messageDate = new Date(message.createdAt);

		// Messages of current user
		if (message.userId === userId) {
			return (
				<div className={"message-card right-align"}>

					<div className={"message-card-info"}>
						<p className={"nickname"}>{message.user}</p>
						<p>{message.text || "        "}</p>
					</div>
					<div className={"message-time"}>
						<i className="far fa-trash-alt icon-inactive"
						   onClick={() => deleteMessage(message.id)}>Delete</i>
						<i className="far fa-edit icon-inactive"
						   onClick={() => this.props.onEdit(message.id, message.text)}>Edit</i>
						<span>{createTimeSpan(messageDate)}</span>
					</div>
				</div>
			);
		}

		// Messages of other users
		return (<div className={"message-card "}>

			<img className={"message-card-avatar"} src={message.avatar} alt="avatar"/>

			<div className={"message-card-info"}>
				<p className={"nickname"}>{message.user}</p>
				<p>{message.text || "        "}</p>
			</div>
			<div className={"message-time"}>
				{message.likes.includes(userId) ?
					<i className="far fa-thumbs-up icon-active"
					   onClick={() => this.deleteLike(userId, message)}><span>{message.likes.length || 0}</span></i>
					:
					<i className="far fa-thumbs-up icon-inactive"
					   onClick={() => this.setLike(userId, message)}><span>{message.likes.length || 0}</span></i>
				}

				<span>{createTimeSpan(messageDate)}</span>
			</div>
		</div>);
	}
}

function createTimeSpan(date) {
	return `${date.getHours()} : ${date.getMinutes()}`;
}

export default MessageCard;
