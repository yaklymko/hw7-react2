import React from "react";

import Header from "./components/Header/Header";
import MessagesList from "./components/MessagesList/MessagesList";
import CreateMessageForm from "./components/CreateMessageForm/CreateMessageForm";
import Loader from "./components/Loader/Loader";
import EditModal from "../Modal/Modal";

import UserService from "../../services/user.service";
import MessageService from "../../services/message.service";

import "./Chat.css";

import {apiUrl} from "../../config";

import {connect} from "react-redux";
import {
	addMessage,
	deleteMessage,
	loadMessages,
	requestMessages,
	setCurrUser,
	updateMessage
} from "../../store/actions/chat";
import {setEditTarget, setVisible} from "../../store/actions/editModal";

class Chat extends React.Component {

	scrollChat = () => {
		const chatBox = document.getElementsByClassName("messages-wrapper")[0];
		chatBox.scrollTop = chatBox.scrollHeight;
	};

	componentDidMount() {
		this.props.setCurrUser(UserService.getCurrUser().id);
		this.props.requestMessages();
		document.addEventListener("keyup", this.onKeyArrow);
		fetch(apiUrl, {
			method: "GET"
		}).then(res => res.json()).then(parsedRes => {
			if (!parsedRes) {
				parsedRes = [];
			}
			parsedRes.forEach(mess => mess.likes = []);
			parsedRes.sort((a,b)=> new Date(a.createdAt) - new Date(b.createdAt));
			this.props.loadMessages(parsedRes);
		});
	}

	

	onKeyArrow = e => {

		if (e.key !== "ArrowUp") return;

		const userMessages = this.props.messages.filter(m => m.userId === this.props.userId);

		if (!userMessages.length) return;
		const lastMessage = userMessages[userMessages.length - 1];

		this.onMessageEdit(lastMessage.id, lastMessage.text);
	};

	componentWillUnmount() {
		document.removeEventListener("keyup", this.onKeyArrow);
	}

	addMessage = (text) => {
		const currUser = UserService.getCurrUser();
		const newMessage = MessageService.createMessage(text, currUser.username, currUser.id);

		this.props.addMessage(newMessage);
		this.setState({rerender: true});
		this.scrollChat();
	};

	editMessage = (id, text) => {
		this.props.updateMessage(id, text);
		this.setState({render: true});
	};

	onMessageEdit = (id, text) => {
		this.props.setEditTarget(id, text);
		this.props.setVisible();
	};

	deleteMessage = (messageId) => {
		this.props.deleteMessage(messageId);
		this.setState({rerender: true});
	};

	render() {

		const messagesArr = this.props.messages;
		const participants = new Set(messagesArr.map(m => m.user));
		const lastMessageAt = messagesArr.length ? messagesArr[messagesArr.length - 1].createdAt : null;

		return (

			<main id={"chatGame"}>
				{this.props.isFetching ? <Loader/> : ""}

				<Header chatName={"My chat"}
					participantsAmount={participants.size}
					messagesAmount={messagesArr.length}
					lastMessageTime={lastMessageAt || (new Date()).toString()}
				/>

				<MessagesList messages={messagesArr} userId={this.props.userId} deleteMessage={this.deleteMessage}
							  onEdit={this.onMessageEdit}/>

				<CreateMessageForm createMessage={this.addMessage} scrollChat={this.scrollChat}/>

				<EditModal modal={this.props.modal} editMessage={this.editMessage} />
			</main>
		);
	}
}


function mapStateToProps(state) {
	return {
		...state.chat,
	};
}

const mapDispatchToProps = (dispatch) => {
	return {
		loadMessages: (m) => dispatch(loadMessages(m)),
		requestMessages: () => dispatch(requestMessages()),
		addMessage: (m) => dispatch(addMessage(m)),
		deleteMessage: (id) => dispatch(deleteMessage(id)),
		setVisible: (m) => dispatch(setVisible(m)),
		setEditTarget: (mId, text) => dispatch(setEditTarget(mId, text)),
		updateMessage: (mId, text) => dispatch(updateMessage(mId, text)),
		setCurrUser: (id) => dispatch(setCurrUser(id)),
	};
};


export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Chat);


