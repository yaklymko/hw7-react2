export const SET_VISIBLE = "SET_VISIBLE";

export const SET_HIDDEN = "SET_HIDDEN";

export const SET_TEXT = "SET_TEXT";

export const SET_TARGET = "SET_TARGET";

export const SAVE_CHANGES = "SAVE_CHANGES";