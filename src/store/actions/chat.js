import * as actionsTypes from "../actionsTypes/chat";

export function loadMessages(messagesArr) {
	return {
		type : actionsTypes.LOAD_MESSAGES,
		messages: messagesArr
	};
}

export function requestMessages(){
	return{
		type : actionsTypes.REQUEST_MESSAGES,
	};
}

export function addMessage(newMessage){
	return{
		type : actionsTypes.ADD_MESSAGE,
		newMessage : newMessage,
	};
}

export function deleteMessage(messageId){
	return{
		type : actionsTypes.DELETE_MESSAGE,
		messageId : messageId,
	};
}

export function updateMessage(messageId, text){
	return{
		type : actionsTypes.UPDATE_MESSAGE,
		messageId,
		text
	};
}

export function setCurrUser(userId){
	return {
		type : actionsTypes.SET_CURR_USER,
		userId
	};
}

