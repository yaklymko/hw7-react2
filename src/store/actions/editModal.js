import * as actionsTypes from "../actionsTypes/editModal";

export function setVisible() {
	return {
		type : actionsTypes.SET_VISIBLE,
	};
}

export function setHidden(){
	return{
		type : actionsTypes.SET_HIDDEN,
	};
}

export function setMessageText(text){
	return{
		type : actionsTypes.SET_TEXT,
		text
	};
}

export function setEditTarget(targetId, targetText){
	return{
		type : actionsTypes.SET_TARGET,
		targetId,
		targetText
	};
}

export function saveChanges(){
	return{
		type : actionsTypes.SAVE_CHANGES,
	};
}
