import * as actionsTypes from "../actionsTypes/editModal";

const initialState = {
	show: false,
	messageText: "",
	currMessageId: null,
};


export default function chatReducer(
	state = initialState,
	action
) {
	switch (action.type) {

	case actionsTypes.SET_VISIBLE:
		return {
			...state,
			show: true,
		};

	case actionsTypes.SET_HIDDEN:
		return {
			...state,
			show: false,
		};

	case actionsTypes.SET_TEXT:
		return {
			...state,
			messageText: action.text,
		};
	
	case actionsTypes.SET_TARGET:

		return {
			...state,
			currMessageId: action.targetId,
			messageText: action.targetText,
		};

	case actionsTypes.SAVE_CHANGES:

		return {
			...state,
		};


	default:
		return state;
	}
}

