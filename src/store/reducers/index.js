import {combineReducers } from "redux";
import messagesReducer from "./chat";
import editModalReducer from "./editModal";

export default  combineReducers({
	chat: messagesReducer,
	editModal : editModalReducer
});


