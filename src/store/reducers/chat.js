import * as actionsTypes from "../actionsTypes/chat";

const initialState = {

	isFetching: false,
	chatName: "MyChatek",
	messages: [],
	userId : 1
};


export default function chatReducer(
	state = initialState,
	action
) {
	switch (action.type) {

	case actionsTypes.LOAD_MESSAGES:
		return {
			...state,
			isFetching: false,
			messages: action.messages,
		};

	case actionsTypes.REQUEST_MESSAGES:
		return {
			...state,
			isFetching: true,
		};

	case actionsTypes.ADD_MESSAGE:
		state.messages.push(action.newMessage);
		return {
			...state,
		};

	case actionsTypes.DELETE_MESSAGE:
		return {
			...state,
			messages: state.messages.filter(m => m.id !== action.messageId)
		};

	case actionsTypes.UPDATE_MESSAGE: {

		const targetMessage = state.messages.find(mess => mess.id === action.messageId);
		targetMessage.text = action.text;
		targetMessage.editedAt = (new Date()).toISOString();

		return {
			...state,
			messages : [...state.messages]
		};
	}

	case actionsTypes.SET_CURR_USER: {
		return {
			...state,
			userId : action.userId
		};
	}


	default:
		return state;
	}
}
