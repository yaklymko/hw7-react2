import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";

import Chat from "./components/Chat/Chat";
import Header from "./components/partials/Header/Header";
import Footer from "./components/partials/Footer/Footer";

import UserService from "./services/user.service";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";

import store from "./store";

UserService.setCurrUser(1, "you");

ReactDOM.render(
	<Provider store={store}>
		<Header/>
		<Chat/>
		<Footer/>
	</Provider>,

	document.getElementById("root")
);
